package com.server;

import com.Dao.DaoTest;
import com.alibaba.fastjson.JSON;
import com.bean.ReceivedBean;

public class Middle {

		public String MiddleTest(String json,DaoTest daoTest){
			try{
				ReceivedBean received = JSON.parseObject(json,ReceivedBean.class);
		    	DataTest dataTest=new DataTest(daoTest);
		    	String data=dataTest.test(received);
		    	return data;
			}catch(Exception e){
			}
			return null;
		}
}
