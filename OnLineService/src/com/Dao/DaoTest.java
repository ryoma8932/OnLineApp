package com.Dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.Util.HibernateUtil;
import com.bean.DataBaseBean;


public class DaoTest {
	private Session session;
	public DaoTest(){
		this.session=HibernateUtil.getSession();
	}
	/**
	 * 最后必须使用的方法
	 */
	public void closeSession(){
		HibernateUtil.closeSession(session);
	}
	/**
	 * 增加用户
	 * @param bean
	 * @return
	 */
	public  int add(DataBaseBean bean){
		try{
		Transaction tx=session.beginTransaction();
		session.save(bean);
		tx.commit();
		return 1;
		}catch(Exception e){
			return 0;
		}
	}
	/**
	 * 更新数据方法
	 */
    public  int update(DataBaseBean bean){  
    	Transaction tx=session.beginTransaction();
    	String hql = "update DataBaseBean u set u.latitude=?,u.longitude=?,u.radius=?,u.addr=?,u.locationDescribe=?,u.speed,=?,u.height=?,u.date=?,u.otherId=? where u.userId=?";  
        Query query = session.createQuery(hql);    
        query.setDouble(0, bean.getLatitude());    
        query.setDouble(1, bean.getLongitude());
        query.setFloat(2, bean.getRadius());
        query.setString(3, bean.getAddr());
        query.setString(4, bean.getLocationDescribe());
        query.setString(5, bean.getSpeed());
        query.setString(6, bean.getHeight());
        query.setString(7, bean.getDate());
        query.setString(8, bean.getOtherId());
        query.setString(9, bean.getUserId());
        int i=query.executeUpdate();  
        tx.commit();
        return i;
    } 
    public int update(DataBaseBean tuser,int id){
    	DataBaseBean tuse = (DataBaseBean)session.get(DataBaseBean.class,id);//根据id获取唯一表行  
          if(tuser != null){  
        	  Transaction tx=session.beginTransaction();
              tuse.setAddr(tuser.getAddr());
              tuse.setOtherId(tuser.getOtherId());
              tuse.setLatitude(tuser.getLatitude());
              tuse.setLongitude(tuser.getLongitude());
              tuse.setLocationDescribe(tuser.getLocationDescribe());
              tuse.setRadius(tuser.getRadius());
              tuse.setHeight(tuser.getHeight());
              tuse.setSpeed(tuser.getSpeed());
              tuse.setDate(tuser.getDate());
              session.update(tuse);  
              tx.commit();
          }   
          //System.out.println("更新成功！updata");
          return 1;   
    } 
    /**
     * 查询数据方法 
     */
    @SuppressWarnings("unchecked")
	public  List<DataBaseBean> query(String user){  
        //注意：此处TUser是类名，而不是数据库的表名,使用数据库表名是查询不到的  
        String hql="from DataBaseBean where userId=?";  
        Query query = session.createQuery(hql);  
        query.setString(0, user);      
        return query.list();    
    } 
    
}
